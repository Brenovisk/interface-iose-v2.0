# Interface IOSE V2.0

Interface com design modificado para que fique com a identidade do produto IOSE. Foi implementado novos ícones, layouts e algumas funcionalidades. 

Principais funcionalidades e modificações implementadas:

    -Mecanismo de pesquisa na tela de quadros e circuitos;
    -Mostra de total de quadros na homepage;
    -Mostra de total de circuitos na homepage-circuit;
    -Mudança de cor e posição do ícone on-off no acionamento dos circuitos com JS;
    -Recuo total do sidebar
    -Responsividade da tabela Scheduler;
    -Mudanças de todos os ícones de modo a corresponder com que se refere;
    -Mudança do layout em todas as telas para melhorar a experiência do usuário;
    -Correção de rota quando clicava em cancelar edição de usuário;

Funcionalidade não implementadas e erros ainda não corrigidos:

    -Erro de rota quando volta da tela de esqueci a senha;
    -Funcionalidade de alterar o agendamento não implementado;
    -"Lembre-me" não funcionando completamente;
    
    